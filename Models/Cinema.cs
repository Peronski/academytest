﻿namespace Test_Cinema.Models {
    public class Cinema {
        public int Id { get; set; }
        public List<SalaCinematografica> SaleCinematografiche { get; set; }

        public void AddSala(SalaCinematografica sala) => SaleCinematografiche.Add(sala);
    }
}
