﻿namespace Test_Cinema.Models {
    public class Biglietto {
        public int Id {get; set;}
        public int Posto {get; set;}
        public float Price {get; set;}
        public int IdFilm { get; set;}
        public int IdSala { get; set;}
    }
}
