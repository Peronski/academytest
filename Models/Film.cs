﻿namespace Test_Cinema.Models {
    public class Film {
        public int Id { get; set; }
        public string Titolo { get; set; }
        public string Autore { get; set; }
        public string Produttore { get; set; }
        public string Genere { get; set; }
        public TimeSpan Durata { get; set; }
    }
}
