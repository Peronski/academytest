﻿using Test_Cinema.Exceptions;

namespace Test_Cinema.Models {
    public class SalaCinematografica {
        public int Id { get; set; }
        public int Capienza { get; set; }
        public int IdFilm { get; set; }

        private int _spettatori = 0;

        public void AddSpettatore() {
            if (_spettatori >= Capienza)
                throw new SalaAlCompletoException();

            _spettatori++;
        }

        public void AddSpettatore(int amount) {
            if (_spettatori + amount > Capienza)
                throw new SalaAlCompletoException("Posti non sufficienti in sala. Sala al completo.");

            _spettatori += amount;
        }

        public void Svuota() {
            _spettatori = 0;
        }
    }
}
