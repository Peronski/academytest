﻿namespace Test_Cinema.Models {
    public class Spettatore {
        public int Id {get; set;}
        public string Nome { get; set;}
        public string Cognome { get; set;}
        public DateTime Birthday { get; set;}
        public int IdBiglietto { get; set;}
    }
}
