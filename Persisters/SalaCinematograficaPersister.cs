﻿using Test_Cinema.Models;
using System.Data.SqlClient;
using Test_Cinema.Retrievers;

namespace Test_Cinema.Persisters {
    public class SalaCinematograficaPersister : IPersister<SalaCinematografica> {
        public SqlConnection Connection { get; set; }
        public IRetriever<SalaCinematografica> Retriever { get; set; }

        public SalaCinematograficaPersister(SqlConnection connection, IRetriever<SalaCinematografica> customerRetriever) {
            Connection = connection;
            Retriever = customerRetriever;
        }

        public bool Add(SalaCinematografica item) {
            string query = @"INSERT INTO [Cinemas].[dbo].[SaleCinematografiche] (Capienza, IdFilm)
                                 VALUES(@Capienza, @IdFilm);";

            SqlCommand sqlCommand = new SqlCommand(query, Connection);

            sqlCommand.Parameters.AddWithValue("@Capienza", item.Capienza);
            sqlCommand.Parameters.AddWithValue("@IdFilm", item.IdFilm);

            //Connection.Open();

            return Convert.ToInt32(sqlCommand.ExecuteScalar()) > 0;
        }

        public bool Delete(int id) {
            if (Retriever.Get(id) != null) {
                string query = @"DELETE FROM [Cinemas].[dbo].[SaleCinematografiche]
                                WHERE [Id] = @id";

                SqlCommand sqlCommand = new SqlCommand(query, Connection);

                sqlCommand.Parameters.AddWithValue("@id", id);

                //Connection.Open();

                return sqlCommand.ExecuteNonQuery() > 0;
            }

            return false;
        }

        public bool Update(SalaCinematografica item, int id) {
            if (Retriever.Get(id) != null) {
                string query = @"UPDATE [Cinemas].[dbo].[SaleCinematografiche] (Capienza, IdFilm)
                                 SET [Capienza] = @Capienza,
                                     [IdFilm] = @IdFilm
                                FROM [Cinemas].[dbo].[SaleCinematografiche]
                                WHERE [Id] = @id";

                SqlCommand sqlCommand = new SqlCommand(query, Connection);

                sqlCommand.Parameters.AddWithValue("@Capienza", item.Capienza);
                sqlCommand.Parameters.AddWithValue("@IdFilm", item.IdFilm);

                //Connection.Open();

                return sqlCommand.ExecuteNonQuery() > 0;
            }

            return Add(item);
        }
    }
}
