﻿using Test_Cinema.Models;
using System.Data.SqlClient;
using Test_Cinema.Retrievers;

namespace Test_Cinema.Persisters {
    public class SpettatorePersister : IPersister<Spettatore> {

        public SqlConnection Connection { get; set; }
        public IRetriever<Spettatore> Retriever { get; set; }

        public SpettatorePersister(SqlConnection connection, IRetriever<Spettatore> customerRetriever) {
            Connection = connection;
            Retriever = customerRetriever;
        }


        public bool Add(Spettatore item) {
            string query = @"INSERT INTO [Cinemas].[dbo].[Spettatori] (Nome, Cognome, Birthday, IdBiglietto)
                                 VALUES(@Nome, @Cognome, @Birthday, @IdBiglietto);";

            SqlCommand sqlCommand = new SqlCommand(query, Connection);

            sqlCommand.Parameters.AddWithValue("@Nome", item.Nome);
            sqlCommand.Parameters.AddWithValue("@Cognome", item.Cognome);
            sqlCommand.Parameters.AddWithValue("@Birthday", item.Birthday);
            sqlCommand.Parameters.AddWithValue("@IdBiglietto", item.IdBiglietto);

            //Connection.Open();

            return Convert.ToInt32(sqlCommand.ExecuteScalar()) > 0;
        }

        public bool Delete(int id) {
            if (Retriever.Get(id) != null) {
                string query = @"DELETE FROM [Cinemas].[dbo].[Spettatore]
                                WHERE [Id] = @id";

                SqlCommand sqlCommand = new SqlCommand(query, Connection);

                sqlCommand.Parameters.AddWithValue("@id", id);

                //Connection.Open();

                return sqlCommand.ExecuteNonQuery() > 0;
            }

            return false;
        }

        public bool Update(Spettatore item, int id) {
            if (Retriever.Get(id) != null) {
                string query = @"UPDATE [Cinemas].[dbo].[Spettatori] (Nome, Cognome, Birthday, IdBiglietto)
                                 SET [Nome] = @Nome,
                                     [Cognome] = @Cognome
                                     [Birthday] = @Birthday,
                                     [IdBiglietto] = @IdBiglietto,
                                FROM [Cinemas].[dbo].[Spettatori]
                                WHERE [Id] = @id";

                SqlCommand sqlCommand = new SqlCommand(query, Connection);

                sqlCommand.Parameters.AddWithValue("@Nome", item.Nome);
                sqlCommand.Parameters.AddWithValue("@Cognome", item.Cognome);
                sqlCommand.Parameters.AddWithValue("@Birthday", item.Birthday);
                sqlCommand.Parameters.AddWithValue("@IdBiglietto", item.IdBiglietto);

                //Connection.Open();

                return sqlCommand.ExecuteNonQuery() > 0;
            }

            return Add(item);
        }
    }
}
