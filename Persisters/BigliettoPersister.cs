﻿using Test_Cinema.Models;
using System.Data.SqlClient;
using Test_Cinema.Retrievers;

namespace Test_Cinema.Persisters {
    public class BigliettoPersister : IPersister<Biglietto> {
        public IRetriever<Biglietto> Retriever { get; set; }
        public SqlConnection Connection { get; set; }

        public BigliettoPersister(SqlConnection connection, IRetriever<Biglietto> customerRetriever) {
            Connection = connection;
            Retriever = customerRetriever;
        }


        public bool Add(Biglietto item) {
            string query = @"INSERT INTO [Cinemas].[dbo].[Biglietti] (Posto, Price, IdFilm, IdSala)
                                 VALUES(@Posto, @Price, @IdFilm, @IdSala);";

            SqlCommand sqlCommand = new SqlCommand(query, Connection);

            sqlCommand.Parameters.AddWithValue("@Posto", item.Posto);
            sqlCommand.Parameters.AddWithValue("@Price", item.Price);
            sqlCommand.Parameters.AddWithValue("@IdFilm", item.IdFilm);
            sqlCommand.Parameters.AddWithValue("@IdSala", item.IdSala);

            //Connection.Open();

            return Convert.ToInt32(sqlCommand.ExecuteScalar()) > 0;
        }

        public bool Delete(int id) {
            if (Retriever.Get(id) != null) {
                string query = @"DELETE FROM [Cinemas].[dbo].[Biglietti]
                                WHERE [Id] = @id";

                SqlCommand sqlCommand = new SqlCommand(query, Connection);

                sqlCommand.Parameters.AddWithValue("@id", id);

                //Connection.Open();

                return sqlCommand.ExecuteNonQuery() > 0;
            }

            return false;
        }

        public bool Update(Biglietto item, int id) {
            if (Retriever.Get(id) != null) {
                string query = @"UPDATE [Cinemas].[dbo].[Biglietti] (Posto, Price, IdFilm, IdSala)
                                 SET [Posto] = @Posto,
                                     [Price] = @Price
                                     [IdFilm] = @IdFilm,
                                     [IdSala] = @IdSala
                                FROM [Cinemas].[dbo].[Biglietti]
                                WHERE [Id] = @id";

                SqlCommand sqlCommand = new SqlCommand(query, Connection);

                sqlCommand.Parameters.AddWithValue("@Posto", item.Posto);
                sqlCommand.Parameters.AddWithValue("@Price", item.Price);
                sqlCommand.Parameters.AddWithValue("@IdFilm", item.IdFilm);
                sqlCommand.Parameters.AddWithValue("@IdSala", item.IdSala);

                Connection.Open();

                return sqlCommand.ExecuteNonQuery() > 0;
            }

            return Add(item);
        }
    }
}
