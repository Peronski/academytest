﻿using Microsoft.AspNetCore.Mvc;
using Test_Cinema.Persisters;
using Test_Cinema.Retrievers;
using Test_Cinema.Models;
using Test_Cinema.Utility;

namespace Test_Cinema.Controllers {
    public class FilmController : Controller, IController<Film> {

        private readonly ILogger<FilmController> _logger;
        public IRetriever<Film> Retriever { get; set; }
        public IPersister<Film> Persister { get; set; }

        public FilmController(IRetriever<Film> retriever, IPersister<Film> persister, ILogger<FilmController> logger) {
            _logger = logger;
            Retriever = retriever;
            Persister = persister;
        }

        // ritorna la view attuale, su cui ci si trova
        public IActionResult Index() {
            return View();
        }

        [HttpGet]
        public IActionResult Get(int Id) {
            if (SafeOperator.SafeOperation(() => Retriever.Get(Id), Retriever.Connection, out Film item))
                return View(item); // view con solo un parametro => view che accetta quel modello result ; // view con due paraetri => view che ha nome specifico e accetta model result  
            return RedirectToAction("Index");
        }

        [HttpPost]
        public void Add(Film product) {
            SafeOperator.SafeOperation(() => Persister.Add(product), Persister.Connection, out bool _);
        }

        [HttpPut]
        public void Update(Film product, int id) {
            SafeOperator.SafeOperation(() => Persister.Update(product, id), Persister.Connection, out bool _);
        }

        [HttpDelete]
        public void Delete(int id) {
            SafeOperator.SafeOperation(() => Persister.Delete(id), Persister.Connection, out bool _);
        }

        [HttpGet]
        public IActionResult GetAll() {
            if (SafeOperator.SafeOperation(() => Retriever.GetAll(), Retriever.Connection, out IEnumerable<Film> items))
                return View(items);
            return RedirectToAction("Index");
        }
    }
}
