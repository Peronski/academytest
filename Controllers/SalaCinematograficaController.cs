﻿using Microsoft.AspNetCore.Mvc;
using Test_Cinema.Persisters;
using Test_Cinema.Retrievers;
using Test_Cinema.Models;
using Test_Cinema.Utility;

namespace Test_Cinema.Controllers {
    public class SalaCinematograficaController : Controller, IController<SalaCinematografica> {
        private readonly ILogger<SalaCinematograficaController> _logger;
        public IRetriever<SalaCinematografica> Retriever { get; set; }
        public IPersister<SalaCinematografica> Persister { get; set; }

        public SalaCinematograficaController(IRetriever<SalaCinematografica> retriever, IPersister<SalaCinematografica> persister, ILogger<SalaCinematograficaController> logger) {
            _logger = logger;
            Retriever = retriever;
            Persister = persister;
        }

        public IActionResult Index() {
            return View();
        }

        [HttpGet]
        public IActionResult Get(int Id) {
            if (SafeOperator.SafeOperation(() => Retriever.Get(Id), Retriever.Connection, out SalaCinematografica item))
                return View(item);

            return RedirectToAction("Index");
        }

        [HttpPost]
        public void Add(SalaCinematografica product) {
            SafeOperator.SafeOperation(() => Persister.Add(product), Persister.Connection, out bool _);
        }

        [HttpPut]
        public void Update(SalaCinematografica product, int id) {
            SafeOperator.SafeOperation(() => Persister.Update(product, id), Persister.Connection, out bool _);
        }

        [HttpDelete]
        public void Delete(int id) {
            SafeOperator.SafeOperation(() => Persister.Delete(id), Persister.Connection, out bool _);
        }

        [HttpGet]
        public IActionResult GetAll() {
            if (SafeOperator.SafeOperation(() => Retriever.GetAll(), Retriever.Connection, out IEnumerable<SalaCinematografica> items))
                return View(items);

            return RedirectToAction("Index");
        }
    }
}
