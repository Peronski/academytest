﻿using Microsoft.AspNetCore.Mvc;
using Test_Cinema.Persisters;
using Test_Cinema.Retrievers;
using Test_Cinema.Models;
using Test_Cinema.Utility;

namespace Test_Cinema.Controllers {
    public class SpettatoreController : Controller, IController<Spettatore>  {

        private readonly ILogger<SpettatoreController> _logger;
        public IRetriever<Spettatore> Retriever { get; set; }
        public IPersister<Spettatore> Persister { get; set; }

        public SpettatoreController(IRetriever<Spettatore> retriever, IPersister<Spettatore> persister, ILogger<SpettatoreController> logger) {
            _logger = logger;
            Retriever = retriever;
            Persister = persister;
        }

        public IActionResult Index() {
            return View();
        }

        [HttpGet]
        public IActionResult Get(int Id) {
            if (SafeOperator.SafeOperation(() => Retriever.Get(Id), Retriever.Connection, out Spettatore item))
                return View(item);

            return RedirectToAction("Index");
        }

        [HttpPost]
        public void Add(Spettatore product) {
            SafeOperator.SafeOperation(() => Persister.Add(product), Persister.Connection, out bool _);
        }

        [HttpPut]
        public void Update(Spettatore product, int id) {
            SafeOperator.SafeOperation(() => Persister.Update(product, id), Persister.Connection, out bool _);
        }

        [HttpDelete]
        public void Delete(int id) {
            SafeOperator.SafeOperation(() => Persister.Delete(id), Persister.Connection, out bool _);
        }

        [HttpGet]
        public IActionResult GetAll() {
            if (SafeOperator.SafeOperation(() => Retriever.GetAll(), Retriever.Connection, out IEnumerable<Spettatore> items))
                return View(items);

            return RedirectToAction("Index");
        }
    }
}
