﻿using Microsoft.AspNetCore.Mvc;
using Test_Cinema.Persisters;
using Test_Cinema.Retrievers;
using Test_Cinema.Models;
using Test_Cinema.Utility;

namespace Test_Cinema.Controllers {
    public class BigliettoController : Controller, IController<Biglietto> {

        private readonly ILogger<BigliettoController> _logger;
        public IRetriever<Biglietto> Retriever { get; set; }
        public IPersister<Biglietto> Persister { get; set; }

        public BigliettoController(IRetriever<Biglietto> retriever, IPersister<Biglietto> persister, ILogger<BigliettoController> logger) {
            _logger = logger;
            Retriever = retriever;
            Persister = persister;
        }

        public IActionResult Index() {
            return View();
        }

        [HttpGet]
        public IActionResult Get(int Id) {
            if (SafeOperator.SafeOperation(() => Retriever.Get(Id), Retriever.Connection, out Biglietto item))
                return View("", item);

            return RedirectToAction("Index");
        }

        [HttpPost]
        public void Add(Biglietto product) {
            SafeOperator.SafeOperation(() => Persister.Add(product), Retriever.Connection, out bool _);
        }

        [HttpPut]
        public void Update(Biglietto product, int id) {
            SafeOperator.SafeOperation(() => Persister.Update(product, id), Persister.Connection, out bool _);
        }

        [HttpDelete]
        public void Delete(int id) {
            SafeOperator.SafeOperation(() => Persister.Delete(id), Persister.Connection, out bool _);
        }

        [HttpGet]
        public IActionResult GetAll() {
            if(SafeOperator.SafeOperation(() => Retriever.GetAll(), Retriever.Connection, out IEnumerable<Biglietto> items))
                return View(items);

            return RedirectToAction("Index");
        }
    }
}
