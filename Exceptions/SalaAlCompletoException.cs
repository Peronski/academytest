﻿namespace Test_Cinema.Exceptions {
    public class SalaAlCompletoException : Exception {
        public SalaAlCompletoException(string message = "Sala al completo.") : base(message) { }
    }
}
