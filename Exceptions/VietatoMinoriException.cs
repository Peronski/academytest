﻿namespace Test_Cinema.Exceptions {
    public class VietatoMinoriException : Exception {
        public VietatoMinoriException(string message = "Eta' minima non raggiunta.") : base() { }
    }
}
