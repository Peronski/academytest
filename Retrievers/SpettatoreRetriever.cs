﻿using Test_Cinema.Models;
using System.Data.SqlClient;

namespace Test_Cinema.Retrievers {
    public class SpettatoreRetriever : IRetriever<Spettatore> {

        public SpettatoreRetriever(SqlConnection connection) {
            Connection = connection;
        }

        public SqlConnection Connection { get; set; }

        public Spettatore Get(int id) {
            string query = @"SELECT [Nome],
                                    [Cognome],
                                    [Birthday],
                                    [IdBiglietto]
                               FROM [Cinemas].[dbo].[Spettatori]
                              WHERE [ID] = @id";

        SqlCommand sqlCommand = new SqlCommand(query, Connection);

            sqlCommand.Parameters.AddWithValue("@id", id);

            //Connection.Open();

            using (SqlDataReader reader = sqlCommand.ExecuteReader()) {
                while (reader.Read()) {
                    return new Spettatore() {
                        Id = id,
                        Nome = reader["Nome"].ToString(),
                        Cognome = reader["Cognome"].ToString(),
                        Birthday = DateTime.Parse(reader["Birthday"].ToString()),
                        IdBiglietto = int.Parse(reader["IdBiglietto"].ToString())
                    };
                }
            }

            return null;
        }

        public IEnumerable<Spettatore> GetAll() {
            string query = @"SELECT [Id],
                                    [Nome],
                                    [Cognome],
                                    [Birthday],
                                    [IdBiglietto]
                             FROM[Cinemas].[dbo].[Spettatori]";

            SqlCommand sqlCommand = new SqlCommand(query, Connection);

            //Connection.Open();

            using (SqlDataReader reader = sqlCommand.ExecuteReader()) {
                while (reader.Read()) {
                    yield return new Spettatore {
                        Id = int.Parse(reader["Id"].ToString()),
                        Nome = reader["Nome"].ToString(),
                        Cognome = reader["Cognome"].ToString(),
                        Birthday = DateTime.Parse(reader["Birthday"].ToString()),
                        IdBiglietto = int.Parse(reader["IdBiglietto"].ToString())
                    };
                }
            }
        }
    }
}
