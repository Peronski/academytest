﻿using Test_Cinema.Models;
using System.Data.SqlClient;

namespace Test_Cinema.Retrievers {
    public class CinemaRetriever : IRetriever<Cinema> {
        public CinemaRetriever(SalaCinematograficaRetriever salaCinematograficaRetriever, SqlConnection connection) {
            SaleRetriever = salaCinematograficaRetriever;
            Connection = connection;
        }

        public SalaCinematograficaRetriever SaleRetriever { get; set; }
        public SqlConnection Connection { get; set; }

        public Cinema Get(int id) {
            string query = @"SELECT [IdSala]
                               FROM [Cinemas].[dbo].[Cinema]
                              WHERE [Id] = @id";

            SqlCommand sqlCommand = new SqlCommand(query, Connection);

            sqlCommand.Parameters.AddWithValue("@id", id);

            //Connection.Open();

            Cinema cinema = new Cinema() { Id = id };

            using (SqlDataReader reader = sqlCommand.ExecuteReader()) {
                while (reader.Read()) {
                   cinema.AddSala(SaleRetriever.Get(int.Parse(reader["IdSala"].ToString())));
                }
                return cinema;
            }

            return null;
        }

        public IEnumerable<Cinema> GetAll() {
            string query = @"SELECT [Id], [IdSala]
                               FROM [Cinemas].[dbo].[Cinema]";

            SqlCommand sqlCommand = new SqlCommand(query, Connection);

            //Connection.Open();

            using (SqlDataReader reader = sqlCommand.ExecuteReader()) {
                while (reader.Read()) {
                    //yield return new Cinema {
                        
                    //};
                }
            }
            return null;
        }
    }
}
