﻿using Test_Cinema.Models;
using System.Data.SqlClient;

namespace Test_Cinema.Retrievers {
    public class BigliettoRetriever : IRetriever<Biglietto> {
        public BigliettoRetriever(SqlConnection connection) {
            Connection = connection;
        }
        public SqlConnection Connection { get; set; }

        public Biglietto Get(int id) {
            string query = @"SELECT [Posto],
                                    [Price],
                                    [IdFilm],
                                    [IdSala]
                               FROM [Cinemas].[dbo].[Biglietti]
                              WHERE [Id] = @id";


        SqlCommand sqlCommand = new SqlCommand(query, Connection);

            sqlCommand.Parameters.AddWithValue("@id", id);

            //Connection.Open();

            using (SqlDataReader reader = sqlCommand.ExecuteReader()) {
                while (reader.Read()) {
                    return new Biglietto() {
                        Id = id,
                        Posto = int.Parse(reader["Posto"].ToString()),
                        Price = float.Parse(reader["Price"].ToString()),
                        IdFilm = int.Parse(reader["IdFilm"].ToString()),
                        IdSala = int.Parse(reader["IdSala"].ToString())
                    };
                }
            }

            return null;
        }

        public IEnumerable<Biglietto> GetAll() {
            string query = @"SELECT [Id],
                                    [Posto],
                                    [Price],
                                    [IdFilm],
                                    [IdSala]
                               FROM [Cinemas].[dbo].[Biglietti]";

            SqlCommand sqlCommand = new SqlCommand(query, Connection);

            //Connection.Open();

            using (SqlDataReader reader = sqlCommand.ExecuteReader()) {
                while (reader.Read()) {
                    yield return new Biglietto {
                        Id = int.Parse(reader["Id"].ToString()),
                        Posto = int.Parse(reader["Posto"].ToString()),
                        Price = float.Parse(reader["Price"].ToString()),
                        IdFilm = int.Parse(reader["IdFilm"].ToString()),
                        IdSala = int.Parse(reader["IdSala"].ToString())
                    };
                }
            }
        }
    }
}
