﻿using Test_Cinema.Models;
using System.Data.SqlClient;

namespace Test_Cinema.Retrievers {
    public class SalaCinematograficaRetriever : IRetriever<SalaCinematografica> {
        public SalaCinematograficaRetriever(SqlConnection connection) {
            Connection = connection;
        }
        public SqlConnection Connection { get; set; }

        public SalaCinematografica Get(int id) {
            string query = @"SELECT [Capienza],
                                    [IdFilm]
                               FROM [Cinemas].[dbo].[SaleCinematografiche]
                              WHERE [Id] = @id";

            SqlCommand sqlCommand = new SqlCommand(query, Connection);

            sqlCommand.Parameters.AddWithValue("@id", id);

            //Connection.Open();

            using (SqlDataReader reader = sqlCommand.ExecuteReader()) {
                while (reader.Read()) {
                    return new SalaCinematografica() {
                        Id = id,
                        Capienza = int.Parse(reader["Capienza"].ToString()),
                        IdFilm = int.Parse(reader["IdFilm"].ToString())
                    };
                }
            }

            return null;
        }

        public IEnumerable<SalaCinematografica> GetAll() {
            string query = @"SELECT [Id],
                                    [Capienza],
                                    [IdFilm]
                               FROM [Cinemas].[dbo].[SaleCinematografiche]";

            SqlCommand sqlCommand = new SqlCommand(query, Connection);

            //Connection.Open();

            using (SqlDataReader reader = sqlCommand.ExecuteReader()) {
                while (reader.Read()) {
                    yield return new SalaCinematografica {
                        Id = int.Parse(reader["Id"].ToString()),
                        Capienza = int.Parse(reader["Capienza"].ToString()),
                        IdFilm = int.Parse(reader["IdFilm"].ToString())
                    };
                }
            }
        }
    }
}
